import React from 'react';

const Option = (props) => (
        <div>
            <p>{props.value}
            <button 
                onClick={(e) => {
                    props.handleDeleteOption( props.value )
                }}>
                Remove
            </button>
            </p>
        </div>
    );
    
export default Option;