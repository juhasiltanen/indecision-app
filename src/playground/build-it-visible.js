
class VisibilityToggle extends React.Component {

    constructor(props) {
        super( props );
        this.clickety = this.clickety.bind( this );
        this.state = 
        {
            title : 'Visibility Toggle',
            showButton : 'Show details',
            hideButton : 'Hide details',
            details : 'Here are some details',
            showDetails : false
        };
    }

    clickety() {
        this.setState( (prevState) => {
            return {
                showDetails : !prevState.showDetails
            }
        });
    }

    render() {
        return(
            <div>
                <h1>{this.state.title}</h1>
                <button onClick={this.clickety}>{this.state.showDetails ? this.state.hideButton : this.state.showButton}</button>
                {this.state.showDetails && (
                    <p>
                    {this.state.details}
                    </p>
                )}
            </div>
        );
    }
}

ReactDOM.render( <VisibilityToggle />, document.getElementById('app'));

/*
var app = {
    title : 'Visibility Toggle',
    showButton : 'Show details',
    hideButton : 'Hide details',
    details : 'Here are some details',
    showDetails : false
};

const approot = document.getElementById("app");

const clickety = () => {
    app.showDetails = !app.showDetails;
    renderPage();
};

const renderPage = () => {
    const template = (
        <div>
            <h1>{app.title}</h1>
            <button onClick={clickety}>{app.showDetails ? app.hideButton : app.showButton}</button>
            {app.showDetails && (
                <p>
                {app.details}
                </p>
            )}
        </div>
    );

    ReactDOM.render( template, approot );
};

renderPage();*/