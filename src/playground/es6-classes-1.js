
class Person {
    constructor(name='Anonymous', age = 0){
        this.name = name;
        this.age = age;
    }

    getDescription() { return `${this.name} is ${this.age} years old` };

    getGreeting() { return `Hello ${this.name}` };
}

class Student extends Person {
    constructor(name, age, major) {
        super(name, age);
        this.major = major;
    }

    hasMajor() {
        return !!this.major;
    }

    getDescription() {
        return this.hasMajor() ? super.getDescription() + ` and has ${this.major} as major` : super.getDescription();
    }
}

class Traveller extends Person {
    constructor(name, age, homeLocation){
        super(name, age);
        this.homeLocation = homeLocation;
    }

    hasHomeLocation() {
        return !!this.homeLocation;
    }

    getGreeting() {
        let description = super.getDescription();

        if( this.hasHomeLocation() ) {
            description += '. I am visiting from '+this.homeLocation;
        }

        return description;
    }
}

const me = new Traveller('Juha Siltanen', 41, 'Turku, Finland');
console.log(me);
console.log(me.getGreeting());
console.log(me.getDescription());

const other = new Traveller();
console.log(other);
console.log(other.getGreeting());
console.log(other.getDescription());