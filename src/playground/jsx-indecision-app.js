console.log('App.js in running!');

// JSX - JavaScript XML

var app = {
    title : 'Indecision App',
    subtitle : 'This is some info',
    options : []
};

const onFormSubmit = (event) => {
    event.preventDefault();

    const option = event.target.elements.option.value;

    //console.log("submitted", option);

    if( option ) {
        app.options.push( option );
        event.target.elements.option.value = '';
        renderPage();
    }
};

const removeAll = (event) => {
    console.log("Remove All clicked");
    app.options = [];
    renderPage();
};

const makeDecision = () => {
    const randomNum = Math.floor( Math.random() * app.options.length );

    console.log(app.options[randomNum]);
};

const approot = document.getElementById("app");
 
const renderPage = () => {
    const template = (
        <div>
            <h1>{app.title}</h1>
            {app.subtitle && <p>{app.subtitle}</p>}
            <p>{app.options.length > 0 ? 'Here are your options' : 'No options'}</p>
            <button disabled={app.options.length===0} onClick={makeDecision}>What should I do?</button>
            <button onClick={removeAll}>Remove All</button>
            <form onSubmit={onFormSubmit}>
                <input type="text" name="option"/>
                <button>Add option</button>
            </form>
            <ol>
            {
                app.options.map( (o) => {
                    return <li key={o}>{o}</li>
                })
            }
            </ol>
            
        </div>
    );
    ReactDOM.render( template, approot );
};

renderPage();
/*
const user = {
    name : 'Juha Siltanen',
    age : 41,
    location : 'Turku, Finland'
}

function getLocation(location) {
    if( location ) {
        return <p>Location: {location}</p>;
    }
}

const template2 = (
    <div>
        <h1>{user.name ? user.name : 'Anonymous'}</h1>
        {user.age >= 18 && <p>Age: {user.age}</p>}
        
        {getLocation(user.location)}
    </div>
);*/



