class Counter extends React.Component {

    componentDidMount() {
        const storedCountString = localStorage.getItem('persistedCounter');
        if( storedCountString ) {
            const persistedCounterValue = parseInt( storedCountString );
            if( persistedCounterValue && isNaN(persistedCounterValue) === false ) {
                this.setState( () => ({count:persistedCounterValue}));
            }
        } else {
            console.log("Nothing in local storage");
        }
    }
    componentDidUpdate(prevProps,prevState){
        if( prevState.count !== this.state.count ){
            localStorage.setItem("persistedCounter",this.state.count);
        } else {
            console.log("Corrupted current count");
        }
    }
    constructor(props){
        super(props);
        this.handleAddOne = this.handleAddOne.bind(this);
        this.handleMinusOne = this.handleMinusOne.bind(this);
        this.handleReset = this.handleReset.bind(this);
        this.state = {
            count : props.count
        };
    }

    handleAddOne() {
        this.setState( (prevState) => {
            return {
                count : prevState.count + 1
            };
        });
    }

    handleMinusOne() {
        this.setState( (prevState) =>{
            return {
                count : prevState.count - 1
            };
        });
    }

    handleReset() {
        this.setState( () => {
            return {
                count : 0
            };
        });
    }

    render() {
        return (
            <div>
                <h1>Count: {this.state.count}</h1>
                <button onClick={this.handleAddOne}>+1</button>
                <button onClick={this.handleMinusOne}>-1</button>
                <button onClick={this.handleReset}>reset</button>
            </div>
        );
    }
}

Counter.defaultProps = {
    count : 0
}

ReactDOM.render( <Counter count={0}/>, document.getElementById('app'));
/*
let count = 0;

const addOne = () => {
    console.log("jep+", count);
    count++;
    renderCountApp();
};

const minusOne = () => {
    console.log("jep-", count);
    count--;
    renderCountApp();
};

const reset = () => {
    console.log("jep 0", count);
    count = 0;
    renderCountApp();
};

const renderCountApp = () => {
    const template2 = (
        <div>
            <h1>Count: {count}</h1>
            <button onClick={addOne}>+1</button>
            <button onClick={minusOne}>-1</button>
            <button onClick={reset}>0</button>
        </div>
    );
    ReactDOM.render(template2,approot);
};

renderCountApp();
*/